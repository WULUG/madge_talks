![Madge Talk](https://gitlab.com/WULUG/madge_talks/-/raw/main/So_many_burgers_I_am_tired_of_eating_them_all..png)
## Links to Talk Notebooks
| Day | Talk Link  |
|--|--|
| 10 Sep 2022 | [Mersenne Twister](https://gitlab.com/WULUG/madge_talks/-/blob/main/mt19937.ipynb)  |
